let app = new Vue({
	el: '#app',
	data() {
		return {
			exg: "SEK",
			selectedFromCurrency: "SEK",
			selectedToCurrency: "USD",
			inputAmount: null,
			outputAmount: null,
			selected: 'SEK',
			options: []
	}
	},
	// When app is created a call to the exchange api is made with a query param of SEK. Then store each object in an array
	created(){
		axios.get('https://api.exchangeratesapi.io/latest?base=SEK')
				.then(resp => {
					for (const [key, val] of Object.entries(resp.data.rates)) {
						let objectToPush = {
							text: key,
							value: val
						}
						this.options.push(objectToPush)
					}
				}).catch( err => {
					console.error(err)
				})
	},
  	methods: {
		  // When user changes the dropdown a call to the api is made and then calculates the response.
		onCurrencyChange () {
			axios.get('https://api.exchangeratesapi.io/latest?base=' + this.selectedFromCurrency)
				.then( resp => {

					let selectedCurrency = Object.entries(resp.data.rates).find(currency => {
						if (currency[0] === this.selectedToCurrency) {
							return currency
						}
					});
		
					this.outputAmount = this.inputAmount * selectedCurrency[1]
				}).catch( err => {
					console.error(err)
				})
		},
		// Toggles between To and From currencies.
		toggleToFrom() {
			let swap = this.selectedFromCurrency;
			this.selectedFromCurrency = this.selectedToCurrency;
			this.selectedToCurrency = swap;
			this.onCurrencyChange();
		},
		// Sets the input value.
		onValueChange(event) {
			this.inputAmount = event.target.value;
			this.onCurrencyChange();
		},
		// Takes one argument and change the 'currency list' depending on which currency user chooses.
		currencySelector(currency) {
			axios.get('https://api.exchangeratesapi.io/latest?base=' + currency)
			.then( resp => {
				this.options = []
				this.selected = currency
				for (const [key, val] of Object.entries(resp.data.rates)) {
					let objectToPush = {
						text: key,
						value: val
					}
					this.options.push(objectToPush)
				}
			}).catch( err => {
				console.error(err)
			})
			
		}
	}
})
